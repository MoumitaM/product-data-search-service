package com.tui.pdss.search.composer.model.location;

public class Airport extends Location
{

   private String countryCode;

   private String icaoCode;

   public String getCountryCode()
   {
      return countryCode;
   }

   public void setCountryCode(String countryCode)
   {
      this.countryCode = countryCode;
   }

   public String getIcaoCode()
   {
      return icaoCode;
   }

   public void setIcaoCode(String icaoCode)
   {
      this.icaoCode = icaoCode;
   }
}
