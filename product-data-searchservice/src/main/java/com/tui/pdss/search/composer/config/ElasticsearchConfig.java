package com.tui.pdss.search.composer.config;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ElasticsearchConfig {
	
	 @Value("${elasticsearch.host}")
	    private String elasticsearchHost;
	 
	 @Value("${elasticsearch.port}")
	 	private String elasticsearchPort;
	 
	 private RestClientBuilder lowLevelRestClient;;
	 private RestHighLevelClient client;

	 	@PostConstruct
	    public void init() {
	        lowLevelRestClient = RestClient.builder(new HttpHost(elasticsearchHost,Integer.parseInt(elasticsearchPort),"https")).setRequestConfigCallback(
	                new RestClientBuilder.RequestConfigCallback() {
	                    @Override
	                    public RequestConfig.Builder customizeRequestConfig(
	                            RequestConfig.Builder requestConfigBuilder) {
	                        return requestConfigBuilder
	                            .setConnectTimeout(60000)
	                            .setSocketTimeout(60000);
	                    }});
	        client = new RestHighLevelClient(lowLevelRestClient);
	    }

	    @PreDestroy
	    public void destroy() throws IOException {
	    	client.close();
	    }

	    @Bean
	    public RestHighLevelClient getClient() {
	        return client;
	    }
}
