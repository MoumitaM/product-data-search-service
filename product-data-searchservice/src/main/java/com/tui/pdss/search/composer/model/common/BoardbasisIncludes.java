package com.tui.pdss.search.composer.model.common;

public enum BoardbasisIncludes {

	BREAKFAST,

	LUNCH,

	DINNER,

	SNACKS,

	DRINKS;

}
