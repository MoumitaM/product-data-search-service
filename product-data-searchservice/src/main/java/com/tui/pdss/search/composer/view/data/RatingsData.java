/**
 *
 */
package com.tui.pdss.search.composer.view.data;

import java.io.Serializable;

public class RatingsData implements Serializable
{

   private static final long serialVersionUID = 1L;

   private String officialRating = "";

   private String tRatingCss;

   private String tripAdvisorRating = "";

   private String customerRating = "";

   private String reviewCount = "";

   /**
    * @return the officialRating
    */
   public String getOfficialRating()
   {
      return officialRating;
   }

   /**
    * @return the tripAdvisorRating
    */
   public String getTripAdvisorRating()
   {
      return tripAdvisorRating;
   }

   /**
    * @param officialRating the officialRating to set
    */
   public void setOfficialRating(final String officialRating)
   {
      this.officialRating = officialRating;
   }

   /**
    * @param tripAdvisorRating the tripAdvisorRating to set
    */
   public void setTripAdvisorRating(final String tripAdvisorRating)
   {
      this.tripAdvisorRating = tripAdvisorRating;
   }

   /**
    * @return the tRatingCss
    */
   public String gettRatingCss()
   {
      return tRatingCss;
   }

   /**
    * @param tRatingCss the tRatingCss to set
    */
   public void settRatingCss(final String tRatingCss)
   {
      this.tRatingCss = tRatingCss;
   }

   /**
    * @return the customerRating
    */
   public String getCustomerRating()
   {
      return customerRating;
   }

   /**
    * @param customerRating the customerRating to set
    */
   public void setCustomerRating(final String customerRating)
   {
      this.customerRating = customerRating;
   }

   /**
    * @return the reviewCount
    */
   public String getReviewCount()
   {
      return reviewCount;
   }

   /**
    * @param reviewCount the reviewCount to set
    */
   public void setReviewCount(final String reviewCount)
   {
      this.reviewCount = reviewCount;
   }

}
