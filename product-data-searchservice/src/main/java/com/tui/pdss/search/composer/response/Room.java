package com.tui.pdss.search.composer.response;

import java.util.List;

public class Room {

	private String roomType;
	private String roomTypeCode;
	private List<FeatureValueSet> featureValueSets;
	private Inventory inventoryId;
	private String seq;
	private int minOccupancy;
	private int maxOccupancy;
	private String roomTitle;
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getRoomTypeCode() {
		return roomTypeCode;
	}
	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}
	public List<FeatureValueSet> getFeatureValueSets() {
		return featureValueSets;
	}
	public void setFeatureValueSets(List<FeatureValueSet> featureValueSets) {
		this.featureValueSets = featureValueSets;
	}
	public Inventory getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Inventory inventoryId) {
		this.inventoryId = inventoryId;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public int getMinOccupancy() {
		return minOccupancy;
	}
	public void setMinOccupancy(int minOccupancy) {
		this.minOccupancy = minOccupancy;
	}
	public int getMaxOccupancy() {
		return maxOccupancy;
	}
	public void setMaxOccupancy(int maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}
	public String getRoomTitle() {
		return roomTitle;
	}
	public void setRoomTitle(String roomTitle) {
		this.roomTitle = roomTitle;
	}
	
	
}
