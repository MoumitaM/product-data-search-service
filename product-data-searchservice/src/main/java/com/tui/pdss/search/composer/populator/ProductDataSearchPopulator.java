package com.tui.pdss.search.composer.populator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import com.tui.pdss.search.composer.response.AccommodationResponse;
import com.tui.pdss.search.composer.response.FeatureValueSet;
import com.tui.pdss.search.composer.response.Media;
import com.tui.pdss.search.composer.view.data.SearchResultAccommodationViewData;

@Component
public class ProductDataSearchPopulator {

	public void populate(AccommodationResponse source,  SearchResultAccommodationViewData target) {
		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setAccomType(source.getType().getCode());
		populateFeatureCodeAndValues(source.getFeatureValueSets(),target);
		populateReviewCount(source,target);
		populateImages(source,target);
	}

	private void populateImages(AccommodationResponse source, SearchResultAccommodationViewData target) {
		target.setImageUrl(source.getPicture().getUrl());
		populateImageUrls(source, target);
	}

	private void populateImageUrls(AccommodationResponse source, SearchResultAccommodationViewData target) {
		List<String> medias=new ArrayList<String>();
		List<Media> mediaList=source.getGalleryImages().stream().filter(image->"medium".equalsIgnoreCase(image.getFormat())).collect(Collectors.toList());
		mediaList.forEach(media->{medias.add(media.getUrl());});
		target.setImageUrls(medias);
	}

	private void populateReviewCount(AccommodationResponse source, SearchResultAccommodationViewData target) {
		target.setTripAdvisorReviewCount(source.getReviewsCount());
	}

	private void populateFeatureCodeAndValues(List<FeatureValueSet> featureValueSets, SearchResultAccommodationViewData target) {
		featureValueSets.forEach(featureValueSet->
		{
			populateLatitude(target, featureValueSet);
			populateLongitude(target, featureValueSet);
			populateOfficialRating(target,featureValueSet);
			populateTripAdvisorRating(target,featureValueSet);
			target.getFeatureCodesAndValues().put(featureValueSet.getFeatureDescriptorCode(),featureValueSet.getValues());
		});	
	}

	private void populateTripAdvisorRating(SearchResultAccommodationViewData target, FeatureValueSet featureValueSet) {
		if("averagetripAdvisorRating".equalsIgnoreCase(featureValueSet.getFeatureDescriptorCode()))
		{
			target.getRatings().setTripAdvisorRating(featureValueSet.getValues().get(0).toString());
		}
		
	}

	private void populateOfficialRating(SearchResultAccommodationViewData target, FeatureValueSet featureValueSet) {
		if("tRating".equalsIgnoreCase(featureValueSet.getFeatureDescriptorCode()))
		{
			target.getRatings().setOfficialRating(featureValueSet.getValues().get(0).toString());
		}
		
	}

	private void populateLongitude(SearchResultAccommodationViewData target, FeatureValueSet featureValueSet) {
		if("longitude".equalsIgnoreCase(featureValueSet.getFeatureDescriptorCode()))
		{
			target.setLongitude(featureValueSet.getValues().get(0).toString());
		}
	}

	private void populateLatitude(SearchResultAccommodationViewData target, FeatureValueSet featureValueSet) {
		if("latitude".equalsIgnoreCase(featureValueSet.getFeatureDescriptorCode()))
		{
			target.setLatitude(featureValueSet.getValues().get(0).toString());
		}
	}

	
}
