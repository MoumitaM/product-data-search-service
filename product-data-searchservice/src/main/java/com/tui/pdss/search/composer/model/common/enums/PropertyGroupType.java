package com.tui.pdss.search.composer.model.common.enums;

public enum PropertyGroupType {

	GENERAL,

	GEO,

	REVIEW,

	RATING,

	PRODUCT_RANGE,

	FACILITY,

	PORT_OF_CALL,

	EDITORIAL,

	MEDIA;

}
