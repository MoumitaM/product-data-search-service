package com.tui.pdss.search.composer.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Thumbnail {

	private String code;
	
	@JsonProperty("URL")
	private String url;
	private String mediaFormat;
    private String mime;
    private String altText;
    private String description;
        
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getMime() {
		return mime;
	}
	public void setMime(String mime) {
		this.mime = mime;
	}
	public String getAltText() {
		return altText;
	}
	public void setAltText(String altText) {
		this.altText = altText;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMediaFormat() {
		return mediaFormat;
	}
	public void setMediaFormat(String mediaFormat) {
		this.mediaFormat = mediaFormat;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
		   
}
