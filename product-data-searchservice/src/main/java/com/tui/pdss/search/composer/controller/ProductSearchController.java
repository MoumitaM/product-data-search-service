/**
 * 
 */
package com.tui.pdss.search.composer.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.tui.pdss.search.composer.facade.ProductSearchFacade;
import com.tui.pdss.search.composer.model.product.Accommodation;
import com.tui.pdss.search.composer.populator.ProductDataSearchPopulator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author moumita.m
 *
 */
@RestController
public class ProductSearchController {

	@Resource
	private ProductSearchFacade productSearchFacade;
	
	@Resource
	private ProductDataSearchPopulator productDataSearchPopulator;

	@GetMapping("/accomodations/{code}")
	public Accommodation retrieveAccomDetails(@PathVariable String code) throws JsonMappingException, JsonProcessingException
	{
		//SearchResultAccommodationViewData accommodationViewData=new SearchResultAccommodationViewData();
		return productSearchFacade.process(code);
		//productDataSearchPopulator.populate(accommodationResponse,accommodationViewData);
		//return accommodationViewData;
	}
	
}
