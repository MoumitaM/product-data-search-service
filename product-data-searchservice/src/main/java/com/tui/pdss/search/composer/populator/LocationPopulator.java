package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Weather;
import com.tui.pdss.search.composer.model.location.Location;

import javax.annotation.Resource;
import java.util.Collections;

public class LocationPopulator implements Populator<String, Location>
{
   @Resource
   private Converter<String, PropertyGroup> propertyGroupConverter;

   @Resource
   private Converter<String, Weather> weatherConverter;

   @Override
   public void populate(String source, Location target) throws ConversionException
   {
      target.setCode("Location_Code");
      target.setDescription("Location_Description");
      target.setIsoCode("Location_IsoCode");
      target.setName("Location_Name");
      target.setParentCode("Location_Parent_Code");
      target.setTitle("Location_Title");
      target.setPropertyGroups(
               propertyGroupConverter.convertAll(Collections.singletonList(source)));
      target.setType("Location_Type");
      target.setWeather(weatherConverter.convert(source));
   }
}
