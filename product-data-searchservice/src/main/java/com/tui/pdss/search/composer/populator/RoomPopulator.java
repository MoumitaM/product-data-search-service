package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.Text;
import com.tui.pdss.search.composer.model.product.Stay;
import com.tui.pdss.search.composer.response.Room;

import javax.annotation.Resource;

public class RoomPopulator implements Populator<Room, Stay>
{

   @Resource
   private Converter<String, Text> localeConverter;

   @Override
   public void populate(Room source, Stay target) throws ConversionException
   {

      target.setId(123);
      target.setCode(source.getRoomTypeCode());
      target.setDescription(localeConverter.convert(source.getRoomTypeCode()));
      target.setPropertyGroups(null);
      target.setTitle(source.getRoomTitle());
      target.setCategory("Room_Category");
      target.setType(source.getRoomType());
      target.setCode(source.getRoomTypeCode());
   }
}
