package com.tui.pdss.search.composer.model.product;

import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Text;

import java.util.List;

public class Stay
{

   private int id;

   private String code;

   private String title;

   private String type;

   private List<PropertyGroup> propertyGroups;

   private String category;

   private Text description;

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public List<PropertyGroup> getPropertyGroups()
   {
      return propertyGroups;
   }

   public void setPropertyGroups(
            List<PropertyGroup> propertyGroups)
   {
      this.propertyGroups = propertyGroups;
   }

   public String getCategory()
   {
      return category;
   }

   public void setCategory(String category)
   {
      this.category = category;
   }

   public Text getDescription()
   {
      return description;
   }

   public void setDescription(Text description)
   {
      this.description = description;
   }
}
