package com.tui.pdss.search.composer.model.common;

import com.tui.pdss.search.composer.model.location.Location;
import com.tui.pdss.search.composer.model.product.Product;

import java.util.List;

public class Content
{

   private String apiVersion;

   private String timeStamp;

   private String status;

   private List<Product> products;

   private List<Location> location;

}
