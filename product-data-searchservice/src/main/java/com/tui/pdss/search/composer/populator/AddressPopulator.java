package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.model.common.Address;

//String as source object will be changed in future once ES domain object is finalised
public class AddressPopulator implements Populator<String, Address>
{
   @Override
   public void populate(String source, Address target) throws ConversionException
   {

      target.setName("Address_name");
      target.setContactName("Address_ContactName");
      target.setAddressOne("Address_addressone");
      target.setAddressTwo("Address_addresstwo");
      target.setAddressThree(3);
      target.setCity("Address_city");
      target.setCountry("Address_country");
      target.setPostCode("Address_postcode");
      target.setLocationURL("Address_locationurl");
      target.setLatitude(123);
      target.setLongitude(456);
      target.setEmail("Address_email");
      target.setContactNumber("Address_contactnumber");
      target.setFax("Address_fax");
      target.setWebsite("Address_website");
      target.setDescription("Address_description");
      target.setOpeningHrs("Address_openinghrs");

   }
}
