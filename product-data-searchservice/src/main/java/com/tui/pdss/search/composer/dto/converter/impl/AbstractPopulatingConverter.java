package com.tui.pdss.search.composer.dto.converter.impl;

import com.tui.pdss.search.composer.populator.Populator;
import com.tui.pdss.search.composer.populator.PopulatorList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class AbstractPopulatingConverter<SOURCE, TARGET> extends AbstractConverter<SOURCE, TARGET>
         implements PopulatorList<SOURCE, TARGET>
{

   private final static Logger LOG = Logger.getLogger(AbstractPopulatingConverter.class);

   private List<Populator<SOURCE, TARGET>> populators;

   @Override
   public void populate(SOURCE source, TARGET target)
   {

      final List<Populator<SOURCE, TARGET>> list = getPopulators();

      if (list == null)
      {
         return;
      }

      for (final Populator<SOURCE, TARGET> populator : list)
      {
         if (populator != null)
         {
            populator.populate(source, target);
         }
      }
   }

   @Override
   public List<Populator<SOURCE, TARGET>> getPopulators()
   {
      return populators;
   }

   @Override
   public void setPopulators(List<Populator<SOURCE, TARGET>> populators)
   {
      this.populators = populators;
   }

   @PostConstruct
   public void removePopulatorsDuplicates()
   {

      if (CollectionUtils.isNotEmpty(this.populators))
      {
         LinkedHashSet<Populator<SOURCE, TARGET>> distinctPopulators = new LinkedHashSet();
         Iterator var3 = this.populators.iterator();

         while (var3.hasNext())
         {
            Populator<SOURCE, TARGET> populator = (Populator) var3.next();
            if (!distinctPopulators.add(populator))
            {
               LOG.warn("Duplicate populator entry [" + populator.getClass().getName() +
                        "] found for converter " + this.getMyBeanName() +
                        "! The duplication has been removed.");
            }
         }

         this.populators = new ArrayList(distinctPopulators);
      }
      else
      {
         LOG.warn("Empty populators list found for converter " + this.getMyBeanName() + "!");
      }

   }
}
