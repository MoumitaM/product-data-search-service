package com.tui.pdss.search.composer.model.product;

import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Text;

import java.util.List;

public class Itinerary
{

   private String code;

   private List<ItineraryLeg> legs;

   private Text name;

   private List<PropertyGroup> propertyGroups;

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public List<ItineraryLeg> getLegs()
   {
      return legs;
   }

   public void setLegs(List<ItineraryLeg> legs)
   {
      this.legs = legs;
   }

   public Text getName()
   {
      return name;
   }

   public void setName(Text name)
   {
      this.name = name;
   }

   public List<PropertyGroup> getPropertyGroups()
   {
      return propertyGroups;
   }

   public void setPropertyGroups(
            List<PropertyGroup> propertyGroups)
   {
      this.propertyGroups = propertyGroups;
   }
}
