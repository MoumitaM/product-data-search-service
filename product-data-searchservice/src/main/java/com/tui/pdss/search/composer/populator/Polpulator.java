package com.tui.pdss.search.composer.populator;

public interface Polpulator<T1, T2> {

	public void populate(T1 source, T2 target);
	
}
