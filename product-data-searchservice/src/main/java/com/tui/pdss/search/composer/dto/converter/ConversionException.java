package com.tui.pdss.search.composer.dto.converter;

public class ConversionException extends RuntimeException {
    public ConversionException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ConversionException(String msg) {
        super(msg);
    }
}
