package com.tui.pdss.search.composer.model.common;

import com.tui.pdss.search.composer.model.common.enums.BrandType;
import com.tui.pdss.search.composer.model.common.enums.PropertyGroupType;

import java.util.List;

public class PropertyGroup
{

   private String id;

   private String code;

   private String name;

   private String title;

   private String url;

   private String description;

   private List<Media> medias;

   private String type;

   private List<Property> properties;

   private PropertyGroupType group;

   private String hashTag;

   private int attributes;

   private List<Season> seasons;

   private String parentCode;

   private List<BrandType> brands;

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public List<Media> getMedias()
   {
      return medias;
   }

   public void setMedias(List<Media> medias)
   {
      this.medias = medias;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public List<Property> getProperties()
   {
      return properties;
   }

   public void setProperties(List<Property> properties)
   {
      this.properties = properties;
   }

   public PropertyGroupType getGroup()
   {
      return group;
   }

   public void setGroup(PropertyGroupType group)
   {
      this.group = group;
   }

   public String getHashTag()
   {
      return hashTag;
   }

   public void setHashTag(String hashTag)
   {
      this.hashTag = hashTag;
   }

   public int getAttributes()
   {
      return attributes;
   }

   public void setAttributes(int attributes)
   {
      this.attributes = attributes;
   }


   public String getParentCode()
   {
      return parentCode;
   }

   public void setParentCode(String parentCode)
   {
      this.parentCode = parentCode;
   }

   public List<BrandType> getBrands()
   {
      return brands;
   }

   public void setBrands(List<BrandType> brands)
   {
      this.brands = brands;
   }

   public List<Season> getSeasons()
   {
      return seasons;
   }

   public void setSeasons(List<Season> seasons)
   {
      this.seasons = seasons;
   }
}
