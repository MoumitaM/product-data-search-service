package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.model.common.WeatherType;

//This is hard-coded populator. once response object is finalised then source object will be changed
public class WeatherTypePopulator implements Populator<String, WeatherType>
{
   @Override
   public void populate(String source, WeatherType target) throws ConversionException
   {

      target.setCode("weatherType_code");
      target.setDescription("weather_description");
      target.setId("weather_id");
      target.setName("weather_name");
      target.setType("weatherType_type");
      target.setAverage("weatherType_average");
      target.setMax("weatherType_max");
      target.setMin("weatherType_min");
      target.setMonth("weatherType_month");
      target.setVariant("weatherType_variant");
   }
}
