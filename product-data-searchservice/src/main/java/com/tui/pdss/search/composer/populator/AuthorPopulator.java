package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.Author;
import com.tui.pdss.search.composer.model.common.Text;

import javax.annotation.Resource;

//String as source object will be changed in future once ES domain object is finalised
public class AuthorPopulator implements Populator<String, Author>
{
   @Resource
   private Converter<String, Text> localeConverter;

   @Override
   public void populate(String source, Author target) throws ConversionException
   {

      target.setName("Author_Name");
      target.setId("Author_ID");
      target.setTitle("Author_Title");
      target.setDescription(localeConverter.convert(source));
      //setting media as null, once source object is known then mediaconverter will be used
      target.setMedia(null);
   }
}
