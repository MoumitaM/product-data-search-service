package com.tui.pdss.search.composer.model.common;

import java.util.List;

public class WeatherType
{

   private String code;

   private String id;

   private String name;

   private String description;

   private String average;

   private String variant;

   private String month;

   private String min;

   private String max;

   private String type;

   private List<Media> media;

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getAverage()
   {
      return average;
   }

   public void setAverage(String average)
   {
      this.average = average;
   }

   public String getVariant()
   {
      return variant;
   }

   public void setVariant(String variant)
   {
      this.variant = variant;
   }

   public String getMonth()
   {
      return month;
   }

   public void setMonth(String month)
   {
      this.month = month;
   }

   public String getMin()
   {
      return min;
   }

   public void setMin(String min)
   {
      this.min = min;
   }

   public String getMax()
   {
      return max;
   }

   public void setMax(String max)
   {
      this.max = max;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public List<Media> getMedia()
   {
      return media;
   }

   public void setMedia(List<Media> media)
   {
      this.media = media;
   }
}
