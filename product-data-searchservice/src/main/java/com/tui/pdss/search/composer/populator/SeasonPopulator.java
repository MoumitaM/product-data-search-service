package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.model.common.Season;

import java.util.Date;

//String as source object will be changed in future once ES domain object is finalised
public class SeasonPopulator implements Populator<String, Season>
{
   @Override
   public void populate(String source, Season target) throws ConversionException
   {

      target.setCode("Season_code");
      target.setStartDate(new Date());
      target.setEndDate(new Date());

   }
}
