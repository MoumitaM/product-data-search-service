package com.tui.pdss.search.composer.facade.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.facade.ProductSearchFacade;
import com.tui.pdss.search.composer.model.product.Accommodation;
import com.tui.pdss.search.composer.response.AccommodationResponse;
import com.tui.pdss.search.composer.service.ProductSearchService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ProductSearchFacadeImpl implements ProductSearchFacade
{

   @Resource
   private ProductSearchService productSearchService;

   @Resource
   private Converter<AccommodationResponse, Accommodation> accommodationConverter;

   @Override
   public Accommodation process(String accomCode)
            throws JsonMappingException, JsonProcessingException
   {
      return accommodationConverter.convert(productSearchService.process(accomCode));
      //return productSearchService.process(accomCode);
   }

}
