package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.model.common.Data;

//String as source object will be changed in future once ES domain object is finalised
public class DataPopulator implements Populator<String, Data>
{

   @Override
   public void populate(String source, Data target) throws ConversionException
   {
      target.setLanguage("Data_language");
      target.setDescription("Data_description");
   }
}
