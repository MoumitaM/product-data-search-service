package com.tui.pdss.search.composer.response;

import java.util.List;

public class TransferTime {

	private List<String> airportNames;

	public List<String> getAirportNames() {
		return airportNames;
	}

	public void setAirportNames(List<String> airportNames) {
		this.airportNames = airportNames;
	}
	
	
}
