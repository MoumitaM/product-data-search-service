package com.tui.pdss.search.composer.model.common;

public class Media
{

   private String id;

   private String alternateName;

   private String title;

   private String url;

   private int priorityOrder;

   private String code;

   private String type;

   private Text caption;

   private String size;

   private String resolution;

   private String mimeType;

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getAlternateName()
   {
      return alternateName;
   }

   public void setAlternateName(String alternateName)
   {
      this.alternateName = alternateName;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   public int getPriorityOrder()
   {
      return priorityOrder;
   }

   public void setPriorityOrder(int priorityOrder)
   {
      this.priorityOrder = priorityOrder;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public Text getCaption()
   {
      return caption;
   }

   public void setCaption(Text caption)
   {
      this.caption = caption;
   }

   public String getSize()
   {
      return size;
   }

   public void setSize(String size)
   {
      this.size = size;
   }

   public String getResolution()
   {
      return resolution;
   }

   public void setResolution(String resolution)
   {
      this.resolution = resolution;
   }

   public String getMimeType()
   {
      return mimeType;
   }

   public void setMimeType(String mimeType)
   {
      this.mimeType = mimeType;
   }
}
