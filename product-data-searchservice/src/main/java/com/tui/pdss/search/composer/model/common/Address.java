package com.tui.pdss.search.composer.model.common;

public class Address
{

   private String name;

   private String contactName;

   private String addressOne;

   private String addressTwo;

   private int addressThree;

   private String city;

   private String country;

   private String postCode;

   private String locationURL;

   private int latitude;

   private int longitude;

   private String email;

   private String contactNumber;

   private String fax;

   private String website;

   private String description;

   private String openingHrs;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getContactName()
   {
      return contactName;
   }

   public void setContactName(String contactName)
   {
      this.contactName = contactName;
   }

   public String getAddressOne()
   {
      return addressOne;
   }

   public void setAddressOne(String addressOne)
   {
      this.addressOne = addressOne;
   }

   public String getAddressTwo()
   {
      return addressTwo;
   }

   public void setAddressTwo(String addressTwo)
   {
      this.addressTwo = addressTwo;
   }

   public int getAddressThree()
   {
      return addressThree;
   }

   public void setAddressThree(int addressThree)
   {
      this.addressThree = addressThree;
   }

   public String getCity()
   {
      return city;
   }

   public void setCity(String city)
   {
      this.city = city;
   }

   public String getCountry()
   {
      return country;
   }

   public void setCountry(String country)
   {
      this.country = country;
   }

   public String getPostCode()
   {
      return postCode;
   }

   public void setPostCode(String postCode)
   {
      this.postCode = postCode;
   }

   public String getLocationURL()
   {
      return locationURL;
   }

   public void setLocationURL(String locationURL)
   {
      this.locationURL = locationURL;
   }

   public int getLatitude()
   {
      return latitude;
   }

   public void setLatitude(int latitude)
   {
      this.latitude = latitude;
   }

   public int getLongitude()
   {
      return longitude;
   }

   public void setLongitude(int longitude)
   {
      this.longitude = longitude;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getContactNumber()
   {
      return contactNumber;
   }

   public void setContactNumber(String contactNumber)
   {
      this.contactNumber = contactNumber;
   }

   public String getFax()
   {
      return fax;
   }

   public void setFax(String fax)
   {
      this.fax = fax;
   }

   public String getWebsite()
   {
      return website;
   }

   public void setWebsite(String website)
   {
      this.website = website;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getOpeningHrs()
   {
      return openingHrs;
   }

   public void setOpeningHrs(String openingHrs)
   {
      this.openingHrs = openingHrs;
   }
}
