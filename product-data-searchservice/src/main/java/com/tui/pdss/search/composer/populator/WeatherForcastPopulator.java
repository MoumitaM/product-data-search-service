package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.model.common.WeatherForecast;

//This is hard-coded populator. once response object is finalised then source object will be changed
public class WeatherForcastPopulator implements Populator<String, WeatherForecast>
{

   @Override
   public void populate(String souce, WeatherForecast target) throws ConversionException
   {
      target.setCode("weather_code");
      target.setDescription("weather_description");
      target.setId("weather_id");
      target.setName("weather_name");
      target.setPressure("weather_pressure");
      target.setRain("weather_rain");
      target.setMaxTemp("weather_max_temperature");
      target.setMinTemp("weather_min_temperature");
      target.setWeatherCondition("weather_condition");
      target.setSource("weather_source");
      target.setWindSpeed("weather_windspeed");
      target.setWindDirection("weather_wind_direction");
   }
}
