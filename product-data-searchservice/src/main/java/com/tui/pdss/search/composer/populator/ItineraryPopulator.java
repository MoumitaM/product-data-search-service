package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.product.Itinerary;
import com.tui.pdss.search.composer.model.product.ItineraryLeg;

import javax.annotation.Resource;
import java.util.Collections;

//String as source object will be changed in future once ES domain object is finalised
public class ItineraryPopulator implements Populator<String, Itinerary>
{
   @Resource
   private Converter<String, ItineraryLeg> itineraryLegConverter;

   @Override

   public void populate(String source, Itinerary target) throws ConversionException
   {
      target.setCode("Itinerary_code");
      target.setLegs(itineraryLegConverter.convertAll(Collections.singletonList(source)));

   }
}
