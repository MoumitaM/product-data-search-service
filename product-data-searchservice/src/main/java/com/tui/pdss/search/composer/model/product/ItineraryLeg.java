package com.tui.pdss.search.composer.model.product;

import java.util.Date;

public class ItineraryLeg
{

   private int arrivalDay;

   private Date arrivalTime;

   private String legCode;

   private String legType;

   private int seq;

   private String type;

   private String parentCode;

   private int departurDay;

   private Date departureTime;

   public int getArrivalDay()
   {
      return arrivalDay;
   }

   public void setArrivalDay(int arrivalDay)
   {
      this.arrivalDay = arrivalDay;
   }

   public Date getArrivalTime()
   {
      return arrivalTime;
   }

   public void setArrivalTime(Date arrivalTime)
   {
      this.arrivalTime = arrivalTime;
   }

   public String getLegCode()
   {
      return legCode;
   }

   public void setLegCode(String legCode)
   {
      this.legCode = legCode;
   }

   public String getLegType()
   {
      return legType;
   }

   public void setLegType(String legType)
   {
      this.legType = legType;
   }

   public int getSeq()
   {
      return seq;
   }

   public void setSeq(int seq)
   {
      this.seq = seq;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public String getParentCode()
   {
      return parentCode;
   }

   public void setParentCode(String parentCode)
   {
      this.parentCode = parentCode;
   }

   public int getDeparturDay()
   {
      return departurDay;
   }

   public void setDeparturDay(int departurDay)
   {
      this.departurDay = departurDay;
   }

   public Date getDepartureTime()
   {
      return departureTime;
   }

   public void setDepartureTime(Date departureTime)
   {
      this.departureTime = departureTime;
   }
}
