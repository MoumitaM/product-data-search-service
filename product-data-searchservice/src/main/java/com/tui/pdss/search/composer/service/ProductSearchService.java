package com.tui.pdss.search.composer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.tui.pdss.search.composer.response.AccommodationResponse;

public interface ProductSearchService {

	AccommodationResponse process(String accomCode) throws JsonMappingException, JsonProcessingException;
}
