package com.tui.pdss.search.composer.model.common;

public class Data
{

   private String description;

   private String language;

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getLanguage()
   {
      return language;
   }

   public void setLanguage(String language)
   {
      this.language = language;
   }
}
