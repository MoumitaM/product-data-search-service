package com.tui.pdss.search.composer.model.product;

import com.tui.pdss.search.composer.model.common.Boardbasis;
import com.tui.pdss.search.composer.model.product.enums.CruiseType;

import java.util.List;

public class Cruise extends Product
{

   private CruiseType type;

   private Boardbasis boardbasis;

   private List<Stay> cabins;

   private List<Itinerary> itineraries;

}
