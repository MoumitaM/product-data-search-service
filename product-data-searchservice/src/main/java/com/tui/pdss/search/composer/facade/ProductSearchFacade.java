package com.tui.pdss.search.composer.facade;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.tui.pdss.search.composer.model.product.Accommodation;

public interface ProductSearchFacade {

	Accommodation process(String accomCode) throws JsonMappingException, JsonProcessingException;
}
