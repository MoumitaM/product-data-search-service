package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.Author;
import com.tui.pdss.search.composer.model.common.Property;
import com.tui.pdss.search.composer.model.common.Season;
import com.tui.pdss.search.composer.model.common.Text;
import com.tui.pdss.search.composer.model.common.enums.PropertyType;

import javax.annotation.Resource;
import java.util.Collections;

//String as source object will be changed in future once ES domain object is finalised
public class PropertyPopulator implements Populator<String, Property>
{
   @Resource
   private Converter<String, Text> localeConverter;

   @Resource
   private Converter<String, Season> seasonConverter;

   @Resource
   private Converter<String, Author> authorConverter;

   @Override
   public void populate(String source, Property target) throws ConversionException
   {
      target.setId("Property_id");
      target.setCode("Property_code");
      target.setTitle("Property_title");
      target.setName(localeConverter.convert(source));
      target.setDescription(localeConverter.convert(source));
      //setting media as null, once source object is known then mediaconverter will be used
      target.setMedias(null);
      target.setUrl("Property_url");
      target.setBody("property_body");
      target.setPropertyType(PropertyType.ACCOMMODATION_TYPE);
      target.setAuthor(authorConverter.convert(source));
      target.setHashtag(Collections.singletonList("Property_Hashtag"));
      target.setMetadata(null);
      target.setSeason(seasonConverter.convert(source));
      target.setPriorityOrder(1);
      target.setDisplayOrder(2);

   }
}
