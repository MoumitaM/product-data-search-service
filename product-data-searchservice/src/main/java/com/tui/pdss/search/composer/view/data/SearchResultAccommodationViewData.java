/**
 *
 */
package com.tui.pdss.search.composer.view.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */

public class SearchResultAccommodationViewData implements Serializable {
	private static final long serialVersionUID = 1L;

	private String code;

	private String name;

	private String imageUrl;

	private List<String> imageUrls;

	private final Map<String, List<String>> imageUrlsMap = new HashMap<String, List<String>>();

	private boolean differentiatedProduct;

	// old diffrnetial type
	private String differentiatedType;

	private String differentiatedCode;

	private String strapline;

	// private SearchResultsLocationData location = new SearchResultsLocationData();

	private Map locationMap = new HashMap<String, String>();

    private RatingsData ratings = new RatingsData();

	// private List<SearchResultRoomsData> rooms = new
	// ArrayList<SearchResultRoomsData>();

	private int tripAdvisorReviewCount;

	private int commercialPriority;

	private final Map<String, List<Object>> featureCodesAndValues = new HashMap<String, List<Object>>();

	// private List<AccomFactsViewData> accomFacts = new ArrayList<>();

	private String latitude;

	private String longitude;

	private Collection<MediaViewData> galleryImages;

	private Map<Integer, List<MediaViewData>> images = new HashMap<Integer, List<MediaViewData>>();

	// private MapPositionsData positionsData = new MapPositionsData();

	private Map<String, String> dataTitle = new HashMap<String, String>();

	private String url;

	private String accomType;

	private boolean isVideoPresent;

	private boolean isGalleryImagesPresent;

	private List<String> multiStayImages;

	private List<String> specialOfferPromotions;

	// private List<BrandType> brands;

	// private BrandType brand;

	private String accomStartDate;

	private String accomEndDate;

	private String accommFormattedStartDate;

	private String accommFormattedEndDate;

	private String singleAccomPageURL;

	private String recScore;

	private String desScore;

	private String fkScore;

	private String disScore;

	private String commValue;

	private String cvType;

	private String system;

	private String score;

	private String minScore;

	private String maxScore;

	private boolean externalAccom;

	private boolean crossBrand;

	private boolean isMapDataPresent;

	private List<String> centreNames;

	// private List<co.uk.tui.accelerrator.facades.view.MapPositionsData>
	// mapPositionsDatas;

	private String promCode;

	private String sellingCode;

	/**
	 * @return the crossBrand
	 */
	public boolean isCrossBrand() {
		return crossBrand;
	}

	/**
	 * @param crossBrand the crossBrand to set
	 */
	public void setCrossBrand(final boolean crossBrand) {
		this.crossBrand = crossBrand;
	}

	/**
	 * @return the externalAccom
	 */
	public boolean isExternalAccom() {
		return externalAccom;
	}

	/**
	 * @param externalAccom the externalAccom to set
	 */
	public void setExternalAccom(final boolean externalAccom) {
		this.externalAccom = externalAccom;
	}

	/**
	 * @return the singleAccomPageURL
	 */
	public String getSingleAccomPageURL() {
		return singleAccomPageURL;
	}

	/**
	 * @param singleAccomPageURL the singleAccomPageURL to set
	 */
	public void setSingleAccomPageURL(final String singleAccomPageURL) {
		this.singleAccomPageURL = singleAccomPageURL;
	}

	/**
	 * @return the specialOfferPromotions
	 */
	public List<String> getSpecialOfferPromotions() {
		return specialOfferPromotions;
	}

	/**
	 * @param specialOfferPromotions the specialOfferPromotions to set
	 */
	public void setSpecialOfferPromotions(final List<String> specialOfferPromotions) {
		this.specialOfferPromotions = specialOfferPromotions;
	}

	/**
	 * @return the isVideoPresent
	 */
	public boolean isVideoPresent() {
		return isVideoPresent;
	}

	/**
	 * @param isVideoPresent the isVideoPresent to set
	 */
	public void setVideoPresent(final boolean isVideoPresent) {
		this.isVideoPresent = isVideoPresent;
	}

	/**
	 * @return the isGalleryImagesPresent
	 */
	public boolean isGalleryImagesPresent() {
		return isGalleryImagesPresent;
	}

	/**
	 * @param isGalleryImagesPresent the isGalleryImagesPresent to set
	 */
	public void setGalleryImagesPresent(final boolean isGalleryImagesPresent) {
		this.isGalleryImagesPresent = isGalleryImagesPresent;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @return the imageUrlsMap
	 */
	public Map<String, List<String>> getImageUrlsMap() {
		return imageUrlsMap;
	}

	/**
	 * @return the differentiatedProduct
	 */
	public boolean isDifferentiatedProduct() {
		return differentiatedProduct;
	}

	/**
	 * @return the differentiatedType
	 */
	public String getDifferentiatedType() {
		return differentiatedType;
	}

	/**
	 * @return the imageUrls
	 */
	public List<String> getImageUrls() {
		return imageUrls;
	}

	/**
	 * @param imageUrls the imageUrls to set
	 */
	public void setImageUrls(final List<String> imageUrls) {
		this.imageUrls = imageUrls;
	}

	/**
	 * @return the strapline
	 */
	public String getStrapline() {
		return strapline;
	}

	/**
	 * @return the location
	 */
	/*
	 * public SearchResultsLocationData getLocation() { return location; }
	 * 
	 */
	/**
	 * @return the ratings
	 */
	
	public RatingsData getRatings()
	{
		return ratings; 
	}
	 
	/**
	 * @return the rooms
	 */
    //public List<SearchResultRoomsData> getRooms() { return rooms; }
			 

	/**
	 * @return the tripAdvisorReviewCount
	 */
	public int getTripAdvisorReviewCount() {
		return tripAdvisorReviewCount;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(final String code) {
		this.code = code;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(final String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @param imageUrlsMap the imageUrlsMap to set
	 */
	public void setImageUrlsMap(final Map<String, List<String>> imageUrlsMap) {

		this.imageUrlsMap.putAll(imageUrlsMap);
	}

	/**
	 * @param differentiatedProduct the differentiatedProduct to set
	 */
	public void setDifferentiatedProduct(final boolean differentiatedProduct) {
		this.differentiatedProduct = differentiatedProduct;
	}

	/**
	 * @param differentiatedType the differentiatedType to set
	 */
	public void setDifferentiatedType(final String differentiatedType) {
		this.differentiatedType = differentiatedType;
	}

	/**
	 * @param strapline the strapline to set
	 */
	public void setStrapline(final String strapline) {
		this.strapline = strapline;
	}

	/**
	 * @param location the location to set
	 */
	/*
	 * public void setLocation(final SearchResultsLocationData location) {
	 * this.location = location; }
	 * 
	 */
	/**
	 * @param ratings the ratings to set
	 */
	 public void setRatings(final RatingsData ratings)
	 { 
		 this.ratings = ratings; 
	 }
	 
	/**
		 * @param rooms the rooms to set
		 *//*
			 * public void setRooms(final List<SearchResultRoomsData> rooms) { this.rooms =
			 * rooms; }
			 */

	/**
	 * @param tripAdvisorReviewCount the tripAdvisorReviewCount to set
	 */
	public void setTripAdvisorReviewCount(final int tripAdvisorReviewCount) {
		this.tripAdvisorReviewCount = tripAdvisorReviewCount;
	}

	/**
	 * @return the featureCodesAndValues
	 */
	public Map<String, List<Object>> getFeatureCodesAndValues() {
		return featureCodesAndValues;
	}

	public void putFeatureCodesAndValues(final Map<String, List<Object>> featureCodesAndValues) {
		this.featureCodesAndValues.putAll(featureCodesAndValues);
	}

//	public String getFeatureValue(final String featureCode) {
//		if (featureCodesAndValues.containsKey(featureCode) && featureCodesAndValues.get(featureCode) != null
//				&& !featureCodesAndValues.get(featureCode).isEmpty()) {
//			return featureCodesAndValues.get(featureCode).get(0).toString();
//		}
//
//		return "";
//	}

	/**
	 * @return the commercialPriority
	 */
	public int getCommercialPriority() {
		return commercialPriority;
	}

	/**
	 * @param commercialPriority the commercialPriority to set
	 */
	public void setCommercialPriority(final int commercialPriority) {
		this.commercialPriority = commercialPriority;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * @return the differentiatedCode
	 */
	public String getDifferentiatedCode() {
		return differentiatedCode;
	}

	/**
	 * @param differentiatedCode the differentiatedCode to set
	 */
	public void setDifferentiatedCode(final String differentiatedCode) {
		this.differentiatedCode = differentiatedCode;
	}

	/**
	 * @return the locationMap
	 */
	public Map getLocationMap() {
		return locationMap;
	}

	/**
	 * @param locationMap the locationMap to set
	 */
	public void setLocationMap(final Map locationMap) {
		this.locationMap = locationMap;
	}

	/**
	 * @return the accomType
	 */
	public String getAccomType() {
		return accomType;
	}

	/**
	 * @param accomType the accomType to set
	 */
	public void setAccomType(final String accomType) {
		this.accomType = accomType;
	}

	/**
	 * @return the galleryImages
	 */
	
	public Collection<MediaViewData> getGalleryImages() { return galleryImages; }
	  
	/**
	 * @param galleryImages the galleryImages to set
	*/
    public void setGalleryImages(final Collection<MediaViewData> galleryImages) {
			  this.galleryImages = galleryImages; }
			 

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(final String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(final String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the images
	 */
	public Map<Integer, List<MediaViewData>> getImages() {
		return images;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(final Map<Integer, List<MediaViewData>> images) {
		this.images = images;
	}

	/**
	 * @return the positionsData
	 */
	/*
	 * public MapPositionsData getPositionsData() { return positionsData; }
	 * 
	 *//**
		 * @param positionsData the positionsData to set
		 *//*
			 * public void setPositionsData(final MapPositionsData positionsData) {
			 * this.positionsData = positionsData; }
			 */

	/**
	 * @return the dataTitle
	 */
	public Map<String, String> getDataTitle() {
		return dataTitle;
	}

	public void setDataTitle(final Map<String, String> dataTitle) {
		this.dataTitle = dataTitle;
	}

	/**
	 * @return the multiStayImages
	 */
	public List<String> getMultiStayImages() {
		return multiStayImages;
	}

	/**
	 * @param multiStayImages the multiStayImages to set
	 */
	public void setMultiStayImages(final List<String> multiStayImages) {
		this.multiStayImages = multiStayImages;
	}

	/**
	 * @return the brands
	 */
	/*
	 * public List<BrandType> getBrands() { return brands; }
	 * 
	 *//**
		 * @param brands the brands to set
		 */
	/*
	 * public void setBrands(final List<BrandType> brands) { this.brands = brands; }
	 * 
	 *//**
		 * @return the brand
		 */
	/*
	 * public BrandType getBrand() { return brand; }
	 * 
	 *//**
		 * @param brand the brand to set
		 *//*
			 * public void setBrand(final BrandType brand) { this.brand = brand; }
			 */

	/**
	 * @return the accomStartDate
	 */
	public String getAccomStartDate() {
		return accomStartDate;
	}

	/**
	 * @param accomStartDate the accomStartDate to set
	 */
	public void setAccomStartDate(final String accomStartDate) {
		this.accomStartDate = accomStartDate;
	}

	/**
	 * Gets the accom end date.
	 *
	 * @return the accom end date
	 */
	public String getAccomEndDate() {
		return accomEndDate;
	}

	/**
	 * Sets the accom end date.
	 *
	 * @param accomEndDate the new accom end date
	 */
	public void setAccomEndDate(final String accomEndDate) {
		this.accomEndDate = accomEndDate;
	}

	/**
	 * Gets the accomm formatted start date.
	 *
	 * @return the accomm formatted start date
	 */
	public String getAccommFormattedStartDate() {
		return accommFormattedStartDate;
	}

	/**
	 * Sets the accomm formatted start date.
	 *
	 * @param accommFormattedStartDate the new accomm formatted start date
	 */
	public void setAccommFormattedStartDate(final String accommFormattedStartDate) {
		this.accommFormattedStartDate = accommFormattedStartDate;
	}

	/**
	 * Gets the accomm formatted end date.
	 *
	 * @return the accomm formatted end date
	 */
	public String getAccommFormattedEndDate() {
		return accommFormattedEndDate;
	}

	/**
	 * Sets the accomm formatted end date.
	 *
	 * @param accommFormattedEndDate the new accomm formatted end date
	 */
	public void setAccommFormattedEndDate(final String accommFormattedEndDate) {
		this.accommFormattedEndDate = accommFormattedEndDate;
	}

	/**
	 * @return the recScore
	 */
	public String getRecScore() {
		return recScore;
	}

	/**
	 * @param recScore the recScore to set
	 */
	public void setRecScore(final String recScore) {
		this.recScore = recScore;
	}

	/**
	 * @return the desScore
	 */
	public String getDesScore() {
		return desScore;
	}

	/**
	 * @param desScore the desScore to set
	 */
	public void setDesScore(final String desScore) {
		this.desScore = desScore;
	}

	/**
	 * @return the fkScore
	 */
	public String getFkScore() {
		return fkScore;
	}

	/**
	 * @param fkScore the fkScore to set
	 */
	public void setFkScore(final String fkScore) {
		this.fkScore = fkScore;
	}

	/**
	 * @return the disScore
	 */
	public String getDisScore() {
		return disScore;
	}

	/**
	 * @param disScore the disScore to set
	 */
	public void setDisScore(final String disScore) {
		this.disScore = disScore;
	}

	/**
	 * @return the commValue
	 */
	public String getCommValue() {
		return commValue;
	}

	/**
	 * @param commValue the commValue to set
	 */
	public void setCommValue(final String commValue) {
		this.commValue = commValue;
	}

	/**
	 * @return the cvType
	 */
	public String getCvType() {
		return cvType;
	}

	/**
	 * @param cvType the cvType to set
	 */
	public void setCvType(final String cvType) {
		this.cvType = cvType;
	}

	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @param system the system to set
	 */
	public void setSystem(final String system) {
		this.system = system;
	}

	/**
	 * @return the score
	 */
	public String getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(final String score) {
		this.score = score;
	}

	/**
	 * @return the minScore
	 */
	public String getMinScore() {
		return minScore;
	}

	/**
	 * @param minScore the minScore to set
	 */
	public void setMinScore(final String minScore) {
		this.minScore = minScore;
	}

	/**
	 * @return the maxScore
	 */
	public String getMaxScore() {
		return maxScore;
	}

	/**
	 * @param maxScore the maxScore to set
	 */
	public void setMaxScore(final String maxScore) {
		this.maxScore = maxScore;
	}

	/**
	 * @return the isMapDataPresent
	 */
	public boolean isMapDataPresent() {
		return isMapDataPresent;
	}

	/**
	 * @param isMapDataPresent the isMapDataPresent to set
	 */
	public void setMapDataPresent(final boolean isMapDataPresent) {
		this.isMapDataPresent = isMapDataPresent;
	}

	public List<String> getCentreNames() {
		return centreNames;
	}

	public void setCentreNames(final List<String> centreNames) {
		this.centreNames = centreNames;
	}

	/**
	 * @return the mapPositionsDatas
	 */
	/*
	 * public List<co.uk.tui.accelerrator.facades.view.MapPositionsData>
	 * getMapPositionsDatas() { return mapPositionsDatas; }
	 * 
	 *//**
		 * @param mapPositionsDatas the mapPositionsDatas to set
		 */
	/*
	 * public void setMapPositionsDatas( final
	 * List<co.uk.tui.accelerrator.facades.view.MapPositionsData> mapPositionsDatas)
	 * { this.mapPositionsDatas = mapPositionsDatas; }
	 * 
	 *//**
		 * @return the accomFacts
		 */
	/*
	 * public List<AccomFactsViewData> getAccomFacts() { return accomFacts; }
	 * 
	 *//**
		 * @param accomFacts the accomFacts to set
		 */
	/*
	 * public void setAccomFacts(final List<AccomFactsViewData> accomFacts) {
	 * this.accomFacts = accomFacts; }
	 * 
	 */
	/**
		 * @return the promCode
		 */
	
	public String getPromCode() { return promCode; }

	 /**
		 * @param promCode the promCode to set
		 */
    public void setPromCode(String promCode) { this.promCode = promCode; }
			 

	/**
	 * @return the sellingCode
	 */
	public String getSellingCode() {
		return sellingCode;
	}

	/**
	 * @param sellingCode the sellingCode to set
	 */
	public void setSellingCode(String sellingCode) {
		this.sellingCode = sellingCode;
	}

}
