package com.tui.pdss.search.composer.model.common;

public class Metadata
{

   private String name;

   private Text title;

   private Text description;

   private Text body;

   private Text keywords;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public Text getTitle()
   {
      return title;
   }

   public void setTitle(Text title)
   {
      this.title = title;
   }

   public Text getDescription()
   {
      return description;
   }

   public void setDescription(Text description)
   {
      this.description = description;
   }

   public Text getBody()
   {
      return body;
   }

   public void setBody(Text body)
   {
      this.body = body;
   }

   public Text getKeywords()
   {
      return keywords;
   }

   public void setKeywords(Text keywords)
   {
      this.keywords = keywords;
   }
}
