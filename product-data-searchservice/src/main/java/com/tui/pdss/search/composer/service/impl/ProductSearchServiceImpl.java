package com.tui.pdss.search.composer.service.impl;

import javax.annotation.Resource;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tui.pdss.search.composer.dao.ProductSearchDao;
import com.tui.pdss.search.composer.response.AccommodationResponse;
import com.tui.pdss.search.composer.service.ProductSearchService;

@Service
public class ProductSearchServiceImpl implements ProductSearchService{

	@Resource
	private ProductSearchDao productSearchDao;

	@Override
	public AccommodationResponse process(String accomCode) throws JsonMappingException, JsonProcessingException 
	{
		ObjectMapper mapper=new ObjectMapper();
		AccommodationResponse res=new AccommodationResponse();
		//to handle the piece of code in future which needs t be populated in viewdata.
		//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		SearchHit[] hit=productSearchDao.findContentByCode(accomCode).getHits().getHits();
		for (SearchHit searchHit : hit) {
			res= mapper.convertValue(searchHit.getSourceAsMap(),AccommodationResponse.class);
		}
		return res;
	}

}
