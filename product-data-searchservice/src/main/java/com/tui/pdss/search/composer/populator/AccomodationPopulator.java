package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.Address;
import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Text;
import com.tui.pdss.search.composer.model.common.Boardbasis;
import com.tui.pdss.search.composer.model.common.Weather;
import com.tui.pdss.search.composer.model.location.Location;
import com.tui.pdss.search.composer.model.product.Accommodation;
import com.tui.pdss.search.composer.model.product.Itinerary;
import com.tui.pdss.search.composer.model.product.Stay;
import com.tui.pdss.search.composer.response.AccommodationResponse;
import com.tui.pdss.search.composer.response.Media;
import com.tui.pdss.search.composer.response.Room;

import javax.annotation.Resource;
import java.util.Collections;

public class AccomodationPopulator implements Populator<AccommodationResponse, Accommodation>
{

   @Resource
   private Converter<Media, com.tui.pdss.search.composer.model.common.Media> mediaConverter;

   @Resource
   private Converter<Room, Stay> roomConverter;

   @Resource
   private Converter<String, Weather> weatherConverter;

   @Resource
   private Converter<String, Itinerary> itineraryConverter;

   @Resource
   private Converter<String, Address> addressConverter;

   @Resource
   private Converter<String, Text> localeConverter;

   @Resource
   private Converter<String, PropertyGroup> propertyGroupConverter;

   @Resource
   private Converter<String, Boardbasis> boardbasisConverter;

   @Resource
   private Converter<String, Location> locationConverter;

   @Override
   public void populate(AccommodationResponse source, Accommodation target)
            throws ConversionException
   {
      //textConverter is hard-coded populator. once response object is finalized then source object will be changed
      target.setName(localeConverter.convert(source.getCode()));
      //textConverter is hard-coded populator. once response object is finalized then source object will be changed
      target.setDescription(localeConverter.convert(source.getCode()));
      target.setCode(source.getCode());
      //target.setMedias(mediaConverter.convertAll(source.getGalleryImages()));
      target.setRooms(roomConverter.convertAll(source.getRooms()));
      //weatherConverter is hard-coded populator. once response object is finalized then source object will be changed
      target.setWeather(weatherConverter.convert("Weather"));
      //itineraryConverter is hard-coded populator. once response object is finalized then source object will be changed
      target.setItineraries(
               itineraryConverter.convertAll(Collections.singletonList(source.getCode())));
      //addressConverter is hard-coded populator. once response object is finalized then source object will be changed
      target.setAddress(addressConverter.convert(source.getCode()));

      //propertyGroupConverter is having source as AccommodationResponse which will change in future. This source object is only used for reference
      target.setPropertyGroups(
               propertyGroupConverter.convertAll(Collections.singleton(source.getCode())));
      target.setBoardbasis(
               boardbasisConverter.convertAll(Collections.singletonList(source.getCode())));
      target.setLocations(
               locationConverter.convertAll(Collections.singletonList(source.getCode())));
   }
}
