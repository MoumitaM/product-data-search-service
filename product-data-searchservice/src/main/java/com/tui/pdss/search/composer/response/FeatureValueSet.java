package com.tui.pdss.search.composer.response;

import java.util.List;

public class FeatureValueSet {
	
	private String featureDescriptorCode;
	private String featureDescriptorName;
	private List<Object> values;
	public String getFeatureDescriptorCode() {
		return featureDescriptorCode;
	}
	public void setFeatureDescriptorCode(String featureDescriptorCode) {
		this.featureDescriptorCode = featureDescriptorCode;
	}
	public String getFeatureDescriptorName() {
		return featureDescriptorName;
	}
	public void setFeatureDescriptorName(String featureDescriptorName) {
		this.featureDescriptorName = featureDescriptorName;
	}
	public List<Object> getValues() {
		return values;
	}
	public void setValues(List<Object> values) {
		this.values = values;
	}
	
	

}
