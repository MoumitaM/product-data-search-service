package com.tui.pdss.search.composer.model.location;

import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Weather;

import java.util.List;

public class Location
{

   private String code;

   private String description;

   private String type;

   private String name;

   private String title;

   private String isoCode;

   private String parentCode;

   private List<PropertyGroup> propertyGroups;

   private Weather Weather;

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getIsoCode()
   {
      return isoCode;
   }

   public void setIsoCode(String isoCode)
   {
      this.isoCode = isoCode;
   }

   public String getParentCode()
   {
      return parentCode;
   }

   public void setParentCode(String parentCode)
   {
      this.parentCode = parentCode;
   }

   public List<PropertyGroup> getPropertyGroups()
   {
      return propertyGroups;
   }

   public void setPropertyGroups(
            List<PropertyGroup> propertyGroups)
   {
      this.propertyGroups = propertyGroups;
   }

   public com.tui.pdss.search.composer.model.common.Weather getWeather()
   {
      return Weather;
   }

   public void setWeather(com.tui.pdss.search.composer.model.common.Weather weather)
   {
      Weather = weather;
   }
}
