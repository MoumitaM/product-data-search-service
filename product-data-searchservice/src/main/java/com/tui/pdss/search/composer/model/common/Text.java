package com.tui.pdss.search.composer.model.common;

import java.util.List;

public class Text
{

   private List<Data> content;

   public List<Data> getContent()
   {
      return content;
   }

   public void setContent(List<Data> content)
   {
      this.content = content;
   }
}
