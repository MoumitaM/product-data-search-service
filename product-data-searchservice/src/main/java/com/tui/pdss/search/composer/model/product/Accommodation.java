package com.tui.pdss.search.composer.model.product;

import com.tui.pdss.search.composer.model.common.Address;
import com.tui.pdss.search.composer.model.common.Boardbasis;

import java.util.List;

public class Accommodation extends Product
{

   private String type;

   private List<Boardbasis> boardbasis;

   private Address address;

   private List<Stay> rooms;

   private List<Itinerary> itineraries;

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public List<Boardbasis> getBoardbasis()
   {
      return boardbasis;
   }

   public void setBoardbasis(List<Boardbasis> boardbasis)
   {
      this.boardbasis = boardbasis;
   }

   public Address getAddress()
   {
      return address;
   }

   public void setAddress(Address address)
   {
      this.address = address;
   }

   public List<Stay> getRooms()
   {
      return rooms;
   }

   public void setRooms(List<Stay> rooms)
   {
      this.rooms = rooms;
   }

   public List<Itinerary> getItineraries()
   {
      return itineraries;
   }

   public void setItineraries(
            List<Itinerary> itineraries)
   {
      this.itineraries = itineraries;
   }

}
