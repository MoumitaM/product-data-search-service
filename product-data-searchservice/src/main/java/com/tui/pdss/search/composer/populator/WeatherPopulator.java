package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.Weather;

import javax.annotation.Resource;
import java.util.Collections;

//This is hard-coded populator. once response object is finalised then source object will be changed
public class WeatherPopulator implements Populator<String, Weather>
{
   @Resource
   private Converter weatherForcastConverter;

   @Resource
   private Converter weatherTypeConverter;

   @Override
   public void populate(String source, Weather target) throws ConversionException
   {
      target.setWeatherForcast(
               weatherForcastConverter.convertAll(Collections.singletonList(source)));
      target.setWeatherType(weatherTypeConverter.convertAll(Collections.singletonList(source)));
   }
}
