package com.tui.pdss.search.composer.dto.converter.impl;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.populator.Populator;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;

public abstract class AbstractConverter<SOURCE, TARGET>
         implements Converter<SOURCE, TARGET>, Populator<SOURCE, TARGET>, InitializingBean,
         BeanNameAware
{

   private Class<TARGET> targetClass;

   private String myBeanName;

   @Override
   public TARGET convert(final SOURCE source) throws ConversionException
   {
      final TARGET target = createFromClass();
      populate(source, target);
      return target;
   }

   @Override
   public TARGET convert(final SOURCE source, final TARGET prototype) throws ConversionException
   {
      populate(source, prototype);
      return prototype;
   }

   protected TARGET createFromClass()
   {
      try
      {
         return targetClass.newInstance();
      }
      catch (final InstantiationException | IllegalAccessException e)
      {
         throw new RuntimeException(e);
      }
   }

   @Override
   public abstract void populate(final SOURCE source, final TARGET target);

   public void setTargetClass(final Class<TARGET> targetClass)
   {
      this.targetClass = targetClass;

      if (targetClass != null)
      {
         createFromClass();
      }
   }

   /*
    * for sanity checks only
    */
   @Override
   public void setBeanName(String name)
   {
      this.myBeanName = name;
   }

   /*
    * Ensures that either a class has been set or createTarget() has been overridden
    */
   @Override
   public void afterPropertiesSet() throws Exception
   {
      if (targetClass == null)
      {
         final Class<? extends AbstractConverter> cl = this.getClass();
         final Method createTargetMethod = ReflectionUtils.findMethod(cl, "createTarget");
         if (AbstractConverter.class.equals(createTargetMethod.getDeclaringClass()))
         {
            throw new IllegalStateException(
                     "Converter '" + myBeanName +
                              "' doesn't have a targetClass property nor does it override createTarget()!");
         }
      }
   }

   public String getMyBeanName()
   {
      return myBeanName;
   }

}
