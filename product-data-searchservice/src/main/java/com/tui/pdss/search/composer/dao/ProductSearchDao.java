package com.tui.pdss.search.composer.dao;

import org.elasticsearch.action.search.SearchResponse;

public interface ProductSearchDao {
	
	SearchResponse findContentByCode(String code);

}
