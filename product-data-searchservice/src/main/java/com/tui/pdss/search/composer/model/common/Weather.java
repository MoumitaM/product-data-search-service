package com.tui.pdss.search.composer.model.common;

import java.util.List;

public class Weather
{

   private List<WeatherType> weatherType;

   private List<WeatherForecast> weatherForcast;

   public List<WeatherType> getWeatherType()
   {
      return weatherType;
   }

   public void setWeatherType(
            List<WeatherType> weatherType)
   {
      this.weatherType = weatherType;
   }

   public List<WeatherForecast> getWeatherForcast()
   {
      return weatherForcast;
   }

   public void setWeatherForcast(
            List<WeatherForecast> weatherForcast)
   {
      this.weatherForcast = weatherForcast;
   }
}
