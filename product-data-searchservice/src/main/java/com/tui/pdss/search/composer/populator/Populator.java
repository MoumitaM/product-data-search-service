package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;

public interface Populator<SOURCE, TARGET> {

	public void populate(SOURCE source, TARGET target) throws ConversionException;
	
}
