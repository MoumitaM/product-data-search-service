package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.model.common.Data;
import com.tui.pdss.search.composer.model.common.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//String as source object will be changed in future once ES domain object is finalised
public class LocalePopulator implements Populator<String, Text>
{

   @Override
   public void populate(String source, Text target) throws ConversionException
   {
      List<Data> dataList = new ArrayList<>();

      //adding source in the list because list of data object is required. this will be changed once es domain object is finalised
      List<String> sourceList = Collections.singletonList(source);
      for (String str : sourceList)
      {
         Data data = new Data();
         data.setLanguage("Data_language");
         data.setDescription("Data_description");
         dataList.add(data);
      }
      target.setContent(dataList);

   }
}
