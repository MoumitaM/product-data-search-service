package com.tui.pdss.search.composer.response;

import java.util.List;

public class Facility {
	
	private String name;
	private String type;
	private Picture picture;
	private String pdfmedia;
	private List<FeatureValueSet> featureValueSets;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Picture getPicture() {
		return picture;
	}
	public void setPicture(Picture picture) {
		this.picture = picture;
	}
	public String getPdfmedia() {
		return pdfmedia;
	}
	public void setPdfmedia(String pdfmedia) {
		this.pdfmedia = pdfmedia;
	}
	public List<FeatureValueSet> getFeatureValueSets() {
		return featureValueSets;
	}
	public void setFeatureValueSets(List<FeatureValueSet> featureValueSets) {
		this.featureValueSets = featureValueSets;
	}
	
	

}
