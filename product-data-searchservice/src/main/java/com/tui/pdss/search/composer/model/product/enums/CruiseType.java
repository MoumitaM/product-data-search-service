package com.tui.pdss.search.composer.model.product.enums;

public enum CruiseType {

	RIVER,

	MULTI_CENTER;

}
