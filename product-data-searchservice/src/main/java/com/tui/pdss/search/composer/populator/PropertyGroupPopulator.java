package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.Property;
import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Season;
import com.tui.pdss.search.composer.model.common.enums.BrandType;
import com.tui.pdss.search.composer.model.common.enums.PropertyGroupType;
import com.tui.pdss.search.composer.response.Media;

import javax.annotation.Resource;
import java.util.Collections;

public class PropertyGroupPopulator implements Populator<String, PropertyGroup>
{
   @Resource
   private Converter<Media, com.tui.pdss.search.composer.model.common.Media> mediaConverter;

   @Resource
   private Converter<String, Season> seasonConverter;

   @Resource
   private Converter<String, Property> propertyConverter;

   @Override

   //AccommodationResponse as source object will be changed in future once ES domain object is finalised
   public void populate(String source, PropertyGroup target)
            throws ConversionException
   {
      target.setId("PropertyGroup_id");
      target.setCode("PropertyGroup_code");
      target.setName("PropertyGroup_name");
      target.setTitle("PropertyGroup_Title");
      target.setUrl("PropertyGroup_Url");
      target.setDescription("PropertyGroup_Description");
      //setting media as null because media converter needs media es domain object. in future this will be changed.
      target.setMedias(null);
      target.setType("PropertyGroup_Type");
      target.setProperties(propertyConverter.convertAll(Collections.singleton(source)));
      target.setGroup(PropertyGroupType.EDITORIAL);
      target.setHashTag("PropertyGroup_HashTag");
      target.setAttributes(0);
      target.setSeasons(seasonConverter.convertAll(Collections.singleton(source)));
      target.setParentCode("PropertyGroup_HashTag");
      target.setBrands(Collections.singletonList(BrandType.TH));

   }
}
