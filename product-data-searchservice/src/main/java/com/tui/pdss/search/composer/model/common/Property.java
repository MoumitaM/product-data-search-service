package com.tui.pdss.search.composer.model.common;

import com.tui.pdss.search.composer.model.common.enums.PropertyType;

import java.util.List;

public class Property
{

   private String id;

   private String code;

   private String title;

   private Text name;

   private Text description;

   private List<Media> medias;

   private String url;

   private PropertyType propertyType;

   private Author author;

   private List<String> hashtag;

   private String body;

   private Metadata metadata;

   private Season season;

   private int priorityOrder;

   private int displayOrder;

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public Text getName()
   {
      return name;
   }

   public void setName(Text name)
   {
      this.name = name;
   }

   public Text getDescription()
   {
      return description;
   }

   public void setDescription(Text description)
   {
      this.description = description;
   }

   public List<Media> getMedias()
   {
      return medias;
   }

   public void setMedias(List<Media> medias)
   {
      this.medias = medias;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   public PropertyType getPropertyType()
   {
      return propertyType;
   }

   public void setPropertyType(PropertyType propertyType)
   {
      this.propertyType = propertyType;
   }

   public Author getAuthor()
   {
      return author;
   }

   public void setAuthor(Author author)
   {
      this.author = author;
   }

   public List<String> getHashtag()
   {
      return hashtag;
   }

   public void setHashtag(List<String> hashtag)
   {
      this.hashtag = hashtag;
   }

   public String getBody()
   {
      return body;
   }

   public void setBody(String body)
   {
      this.body = body;
   }

   public Metadata getMetadata()
   {
      return metadata;
   }

   public void setMetadata(Metadata metadata)
   {
      this.metadata = metadata;
   }

   public Season getSeason()
   {
      return season;
   }

   public void setSeason(Season season)
   {
      this.season = season;
   }

   public int getPriorityOrder()
   {
      return priorityOrder;
   }

   public void setPriorityOrder(int priorityOrder)
   {
      this.priorityOrder = priorityOrder;
   }

   public int getDisplayOrder()
   {
      return displayOrder;
   }

   public void setDisplayOrder(int displayOrder)
   {
      this.displayOrder = displayOrder;
   }
}
