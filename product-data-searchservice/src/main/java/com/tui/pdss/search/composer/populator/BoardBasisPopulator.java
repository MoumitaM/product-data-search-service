package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.dto.converter.Converter;
import com.tui.pdss.search.composer.model.common.Boardbasis;
import com.tui.pdss.search.composer.model.common.BoardbasisIncludes;
import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Text;

import javax.annotation.Resource;
import java.util.Collections;

//String as source object will be changed in future once ES domain object is finalised
public class BoardBasisPopulator implements Populator<String, Boardbasis>
{
   @Resource
   private Converter<String, Text> localeConverter;

   @Resource
   private Converter<String, PropertyGroup> propertyGroupConverter;

   @Override
   public void populate(String source, Boardbasis target) throws ConversionException
   {
      target.setCode("BoardBasis_Code");
      target.setId("BoardBasis_Id");
      target.setName("BoardBasis_Name");
      target.setDescription(localeConverter.convert(source));
      target.setTitle(localeConverter.convert(source));
      target.setPropertyGroups(
               propertyGroupConverter.convertAll(Collections.singletonList(source)));
      target.setBoardbasisIncludes(Collections.singletonList(BoardbasisIncludes.DINNER));

   }
}
