package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.response.Media;

public class MediaPopulator implements Populator<Media, com.tui.pdss.search.composer.model.common.Media>
{
   @Override
   public void populate(Media source, com.tui.pdss.search.composer.model.common.Media target)
            throws ConversionException
   {
      target.setCode(source.getCode());
      target.setUrl(source.getUrl());
      target.setSize(source.getFormat());
      //target.setCaption(source.getCaption());
      target.setMimeType("application/octet-stream");

   }
}
