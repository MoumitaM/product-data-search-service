package com.tui.pdss.search.composer.dao.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.annotation.Resource;

import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tui.pdss.search.composer.config.ElasticsearchConfig;
import com.tui.pdss.search.composer.dao.ProductSearchDao;
import com.tui.pdss.search.composer.response.AccommodationResponse;

@Component
public class ProductSearchDaoImpl implements ProductSearchDao {

	@Autowired
	private RestHighLevelClient client;

	@Override
	public SearchResponse findContentByCode(String code) {
		SearchResponse searchResponse = null;
		QueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("code", code);
		SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
		sourceBuilder.query(matchQueryBuilder);
		SearchRequest searchRequest = new SearchRequest("code");
		searchRequest.source(sourceBuilder);
		try {
			searchResponse = client.search(searchRequest,RequestOptions.DEFAULT);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return searchResponse;
	}

}
