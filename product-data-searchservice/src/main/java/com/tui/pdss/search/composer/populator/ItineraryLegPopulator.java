package com.tui.pdss.search.composer.populator;

import com.tui.pdss.search.composer.dto.converter.ConversionException;
import com.tui.pdss.search.composer.model.product.ItineraryLeg;

import java.util.Date;

//String as source object will be changed in future once ES domain object is finalised
public class ItineraryLegPopulator implements Populator<String, ItineraryLeg>
{
   @Override
   public void populate(String source, ItineraryLeg target) throws ConversionException
   {
      target.setArrivalDay(1);
      target.setArrivalTime(new Date());
      target.setLegCode("ItineraryLeg_code");
      target.setLegType("ItineraryLeg_Leg_type");
      target.setSeq(0);
      target.setType("ItineraryLeg_type");
      target.setParentCode("ItineraryLeg_parent_Code");

   }
}
