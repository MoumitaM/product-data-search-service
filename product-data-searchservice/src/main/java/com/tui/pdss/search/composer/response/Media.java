package com.tui.pdss.search.composer.response;

public class Media {

	private String caption;
	private String code;
	private String url;
	private String internalUrl;
	private String format;
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getInternalUrl() {
		return internalUrl;
	}
	public void setInternalUrl(String internalUrl) {
		this.internalUrl = internalUrl;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	
	
}
