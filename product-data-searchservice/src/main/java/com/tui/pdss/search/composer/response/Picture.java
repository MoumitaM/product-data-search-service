package com.tui.pdss.search.composer.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Picture {

    private String code;
    
    @JsonProperty("URL")
    private String url;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getUrl() { 
		 return url; 
	} 
	public void setUrl(String url) {
		 this.url = url;
	}
	 	  
}
