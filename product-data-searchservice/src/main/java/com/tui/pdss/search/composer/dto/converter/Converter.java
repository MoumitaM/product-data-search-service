package com.tui.pdss.search.composer.dto.converter;

import org.springframework.util.CollectionUtils;

import java.util.*;

public interface Converter<SOURCE, TARGET>
         extends org.springframework.core.convert.converter.Converter<SOURCE, TARGET>
{
   TARGET convert(SOURCE source) throws ConversionException;

   TARGET convert(SOURCE source, TARGET target) throws ConversionException;

   default List<TARGET> convertAll(Collection<? extends SOURCE> sources) throws ConversionException
   {
      if (CollectionUtils.isEmpty(sources))
      {
         return Collections.emptyList();
      }
      else
      {
         List<TARGET> result = new ArrayList(sources.size());
         Iterator itr = sources.iterator();

         while (itr.hasNext())
         {
            SOURCE source = (SOURCE) itr.next();
            result.add(this.convert(source));
         }

         return result;
      }
   }

}
