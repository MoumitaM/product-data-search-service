package com.tui.pdss.search.composer.model.common;

import java.util.List;

public class Author
{

   private String id;

   private String name;

   private Text description;

   private List<Media> media;

   private String title;

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public Text getDescription()
   {
      return description;
   }

   public void setDescription(Text description)
   {
      this.description = description;
   }

   public List<Media> getMedia()
   {
      return media;
   }

   public void setMedia(List<Media> media)
   {
      this.media = media;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }
}
