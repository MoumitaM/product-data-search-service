package com.tui.pdss.search.composer.model.product;

import com.tui.pdss.search.composer.model.common.PropertyGroup;
import com.tui.pdss.search.composer.model.common.Text;
import com.tui.pdss.search.composer.model.common.Weather;
import com.tui.pdss.search.composer.model.location.Location;

import java.util.Date;
import java.util.List;

public class Product
{

   private String code;

   private String id;

   private Text name;

   private Text description;

   private Date lastUpdated;

   private int status;

   private String type;

   private Text title;

   private String url;

   private List<PropertyGroup> propertyGroups;

   private List<Location> locations;

   private String hashTag;

   private Weather weather;

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public Text getName()
   {
      return name;
   }

   public void setName(Text name)
   {
      this.name = name;
   }

   public Text getDescription()
   {
      return description;
   }

   public void setDescription(Text description)
   {
      this.description = description;
   }

   public Date getLastUpdated()
   {
      return lastUpdated;
   }

   public void setLastUpdated(Date lastUpdated)
   {
      this.lastUpdated = lastUpdated;
   }

   public int getStatus()
   {
      return status;
   }

   public void setStatus(int status)
   {
      this.status = status;
   }

   public String getType()
   {
      return type;
   }

   public void setType(String type)
   {
      this.type = type;
   }

   public Text getTitle()
   {
      return title;
   }

   public void setTitle(Text title)
   {
      this.title = title;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   public List<PropertyGroup> getPropertyGroups()
   {
      return propertyGroups;
   }

   public void setPropertyGroups(
            List<PropertyGroup> propertyGroups)
   {
      this.propertyGroups = propertyGroups;
   }

   public List<Location> getLocations()
   {
      return locations;
   }

   public void setLocations(List<Location> locations)
   {
      this.locations = locations;
   }

   public String getHashTag()
   {
      return hashTag;
   }

   public void setHashTag(String hashTag)
   {
      this.hashTag = hashTag;
   }

   public Weather getWeather()
   {
      return weather;
   }

   public void setWeather(Weather weather)
   {
      this.weather = weather;
   }
}
