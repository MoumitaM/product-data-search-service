package com.tui.pdss.search.composer.response;

import java.util.ArrayList;
import java.util.List;

public class AccommodationResponse {

	 private String code;
	 
	 private String name;
	 
	 private Type type;
	 
	 private double priceQuantity;

	 private String ratingsBar;
	 
	 private double reviewRating;
	 
	 private String reviewRatingUrl;
	 
	 private int reviewsCount;
	 
	 private Picture picture;
	 
	 private Thumbnail thumbnail;
	 
	 private List<FeatureValueSet> featureValueSets=new ArrayList<FeatureValueSet>();
	 
	 private List<Media> galleryImages;
	 
	 private TransferTime transferTimes;
	 
	 private List<Facility> facilities;
	 
	 private List<String> superCategories;
	 
	 private List<Room> rooms;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public double getPriceQuantity() {
		return priceQuantity;
	}

	public void setPriceQuantity(double priceQuantity) {
		this.priceQuantity = priceQuantity;
	}

	public String getRatingsBar() {
		return ratingsBar;
	}

	public void setRatingsBar(String ratingsBar) {
		this.ratingsBar = ratingsBar;
	}

	public double getReviewRating() {
		return reviewRating;
	}

	public void setReviewRating(double reviewRating) {
		this.reviewRating = reviewRating;
	}

	public String getReviewRatingUrl() {
		return reviewRatingUrl;
	}

	public void setReviewRatingUrl(String reviewRatingUrl) {
		this.reviewRatingUrl = reviewRatingUrl;
	}

	public int getReviewsCount() {
		return reviewsCount;
	}

	public void setReviewsCount(int reviewsCount) {
		this.reviewsCount = reviewsCount;
	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}

	public List<FeatureValueSet> getFeatureValueSets() {
		return featureValueSets;
	}

	public void setFeatureValueSets(List<FeatureValueSet> featureValueSets) {
		this.featureValueSets = featureValueSets;
	}

	public List<Media> getGalleryImages() {
		return galleryImages;
	}

	public void setGalleryImages(List<Media> galleryImages) {
		this.galleryImages = galleryImages;
	}

	public TransferTime getTransferTimes() {
		return transferTimes;
	}

	public void setTransferTimes(TransferTime transferTimes) {
		this.transferTimes = transferTimes;
	}

	public List<Facility> getFacilities() {
		return facilities;
	}

	public void setFacilities(List<Facility> facilities) {
		this.facilities = facilities;
	}

	public List<String> getSuperCategories() {
		return superCategories;
	}

	public void setSuperCategories(List<String> superCategories) {
		this.superCategories = superCategories;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}
	
	
	
}
