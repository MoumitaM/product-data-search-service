package com.tui.pdss.search.composer.model.common;

import java.util.List;

public class WeatherForecast
{

   private String code;

   private String id;

   private String name;

   private String description;

   private String maxTemp;

   private String minTemp;

   private String pressure;

   private String rain;

   private String weatherCondition;

   private String windDirection;

   private String windSpeed;

   private String source;

   private List<Media> medias;

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getMaxTemp()
   {
      return maxTemp;
   }

   public void setMaxTemp(String maxTemp)
   {
      this.maxTemp = maxTemp;
   }

   public String getMinTemp()
   {
      return minTemp;
   }

   public void setMinTemp(String minTemp)
   {
      this.minTemp = minTemp;
   }

   public String getPressure()
   {
      return pressure;
   }

   public void setPressure(String pressure)
   {
      this.pressure = pressure;
   }

   public String getRain()
   {
      return rain;
   }

   public void setRain(String rain)
   {
      this.rain = rain;
   }

   public String getWeatherCondition()
   {
      return weatherCondition;
   }

   public void setWeatherCondition(String weatherCondition)
   {
      this.weatherCondition = weatherCondition;
   }

   public String getWindDirection()
   {
      return windDirection;
   }

   public void setWindDirection(String windDirection)
   {
      this.windDirection = windDirection;
   }

   public String getWindSpeed()
   {
      return windSpeed;
   }

   public void setWindSpeed(String windSpeed)
   {
      this.windSpeed = windSpeed;
   }

   public String getSource()
   {
      return source;
   }

   public void setSource(String source)
   {
      this.source = source;
   }

   public List<Media> getMedias()
   {
      return medias;
   }

   public void setMedias(List<Media> medias)
   {
      this.medias = medias;
   }
}
