package com.tui.pdss.search.composer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchRestClientAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@EnableAutoConfiguration(exclude={ElasticsearchRestClientAutoConfiguration.class})
@ImportResource("classpath:product-data-searchservice-spring.xml")
public class ProductDataSearchserviceApplication {

	public static void main(String[] args) {
		System.out.println("Start spring boot app...");
		SpringApplication.run(ProductDataSearchserviceApplication.class, args);
	}

}
