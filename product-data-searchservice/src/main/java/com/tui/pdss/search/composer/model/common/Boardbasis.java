package com.tui.pdss.search.composer.model.common;

import java.util.List;

public class Boardbasis
{

   private String code;

   private String id;

   private String name;

   private String url;

   private Text description;

   private Text title;

   private List<BoardbasisIncludes> boardbasisIncludes;

   private List<PropertyGroup> propertyGroups;

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   public Text getDescription()
   {
      return description;
   }

   public void setDescription(Text description)
   {
      this.description = description;
   }

   public Text getTitle()
   {
      return title;
   }

   public void setTitle(Text title)
   {
      this.title = title;
   }

   public List<BoardbasisIncludes> getBoardbasisIncludes()
   {
      return boardbasisIncludes;
   }

   public void setBoardbasisIncludes(
            List<BoardbasisIncludes> boardbasisIncludes)
   {
      this.boardbasisIncludes = boardbasisIncludes;
   }

   public List<PropertyGroup> getPropertyGroups()
   {
      return propertyGroups;
   }

   public void setPropertyGroups(
            List<PropertyGroup> propertyGroups)
   {
      this.propertyGroups = propertyGroups;
   }
}
