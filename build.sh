#!/sh
# -----------------------------------------------------------------------------
# Run Script for the PDSS build
#

ABS_DIR="/usr/bin/pdss/product-data-search-service/product-data-searchservice"
BRANCH_NAME="master"
JAVA_HOME="/usr/lib/jvm/java-11-amazon-corretto.x86_64"

echo "--------------------------------------Update repo-----------------------------------------------------"
(git clean -d -f && git checkout * && git pull origin $BRANCH_NAME)
cd $ABS_DIR

echo "--------------------------------------Set env path-----------------------------------------"
export JAVA_HOME=$JAVA_HOME
export PATH=$JAVA_HOME/bin:$PATH

echo "--------------------------------------Start spring boot application---------------------------------------------"
nohup mvn spring-boot:run > $ABS_DIR/startup.log &


